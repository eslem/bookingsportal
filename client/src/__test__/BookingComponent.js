import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import RowComponent from '../components/RowComponent';

import mock from './mock';
const bookings = mock.bookings;

let container;

it('can render the rowsr', () => {

    const div = document.createElement('div');
    ReactDOM.render(<RowComponent book={bookings[0]} />, div);
    ReactDOM.unmountComponentAtNode(div);

});