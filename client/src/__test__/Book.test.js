import Booking from '../components/Booking';

import mock from './mock';
const bookings = mock.bookings;

it('Book', () => {
    const book1 = new Booking(bookings[0]);
    const book2 = new Booking(bookings[1]);
    const book3= new Booking(bookings[2]);

    const book_error_email = new Booking(bookings[8]);

    expect(book1.ammount_feeds).toBe(1030)
    expect(book2.ammount_feeds).toBe(105)
    expect(book3.ammount_feeds).toBe(1020204)

    expect(book3.quality_errors.length).toBe(1);
    expect(book_error_email.quality_errors.length).toBe(1);
});