const bookings = {
    "bookings": [
        {
            "reference": "86516867",
            "amount": 1000,
            "amount_received": 1200,
            "country_from": "Takodana",
            "sender_full_name": "Nute Gunray",
            "sender_address": "80288 Hintz Mountain",
            "school": "West Hilll College",
            "currency_from": "CAD",
            "student_id": 877681,
            "email": "mariela@raukshlerin.io"
        },
        {
            "reference": "48274024",
            "amount": 100,
            "amount_received": 77187377,
            "country_from": "Naboo",
            "sender_full_name": "Anakin Skywalker",
            "sender_address": "357 Bradtke Hills",
            "school": "South Romaguera College",
            "currency_from": "CAD",
            "student_id": 115744,
            "email": "marta@beier.name"
        },
        {
            "reference": "45180386",
            "amount": 1000200,
            "amount_received": 132433634,
            "country_from": "Hosnian Prime",
            "sender_full_name": "Obi-Wan Kenobi",
            "sender_address": "7427 Joaquin Brook",
            "school": "Western Sanford",
            "currency_from": "EUR",
            "student_id": 795196,
            "email": "karl.kuphal@kuhn.info"
        },
        {
            "reference": "135437103",
            "amount": 31536141,
            "amount_received": 31536150,
            "country_from": "Mustafar",
            "sender_full_name": "Lando Calrissian",
            "sender_address": "5395 Kenya Rue",
            "school": "Eastern Nevada Academy",
            "currency_from": "EUR",
            "student_id": 362443,
            "email": "lorenza.nader@medhurst.org"
        },
        {
            "reference": "15554547",
            "amount": 111668074,
            "amount_received": 111668071,
            "country_from": "Kamino",
            "sender_full_name": "Darth Caedus",
            "sender_address": "83174 Kunze Hollow",
            "school": "Rice Academy",
            "currency_from": "USD",
            "student_id": 250562,
            "email": "lauren_ruecker@quitzon.info"
        },
        {
            "reference": "21370261",
            "amount": 121564555,
            "amount_received": 121564545,
            "country_from": "Tatooine",
            "sender_full_name": "Jar Jar Binks",
            "sender_address": "69975 Mollie Prairie",
            "school": "West Virginia College",
            "currency_from": "USD",
            "student_id": 610062,
            "email": "savanna@tillmansmitham.biz"
        },
        {
            "reference": "114385746",
            "amount": 223,
            "amount_received": 219,
            "country_from": "Spain",
            "sender_full_name": "hg g",
            "sender_address": "Wellinngton Road 37",
            "school": "Arizona",
            "currency_from": "usd",
            "student_id": 0,
            "email": "j.eslem03@gmail.com"
        },
        {
            "reference": "80231343",
            "amount": 100,
            "amount_received": 91,
            "country_from": "Spain",
            "sender_full_name": "test100",
            "sender_address": "test100",
            "school": "MIT",
            "currency_from": "eur",
            "student_id": 0,
            "email": "test100@test.com"
        },
        {
            "reference": "62371468",
            "amount": 12,
            "amount_received": 12.6,
            "country_from": "Spain",
            "sender_full_name": "test100",
            "sender_address": "test100",
            "school": "MIT",
            "currency_from": "eur",
            "student_id": 0,
            "email": "test100test.com"
        }
    ]
}

export default bookings;