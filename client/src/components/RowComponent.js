import React from 'react';
import PropTypes from 'prop-types';
import Booking from './Booking';

class RowComponent extends React.Component {
    static propsTypes = {
        book: PropTypes.object
    }

    render() {
        const bookObject = new Booking(this.props.book);

        let over = "-";
        let under = "-"

        if (bookObject.amount_received > bookObject.ammount_feeds)
            over = "x"

        if (bookObject.amount_received < bookObject.ammount_feeds)
            under = "x"


        let checks = "-"

        if (bookObject.quality_errors.length > 0) {
            checks = bookObject.quality_errors.map((item, i) => (
                <p key={i}>{item}</p>
            ));
        }

        return (
            <tr>
                <td>{bookObject.reference}</td>
                <td>{bookObject.amount}</td>
                <td>{bookObject.ammount_feeds}</td>
                <td>{bookObject.amount_received}</td>
                <td>{
                    checks
                }</td>
                <td>{over}</td>
                <td>{under}</td>
            </tr>
        )
    }
}


export default RowComponent;