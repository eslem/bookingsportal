import React from 'react';
import RowComponent from './RowComponent';
import PropTypes from 'prop-types';
import './TableComponent.css'


class TableComponent extends React.Component {
    static propsTypes = {
        bookings: PropTypes.array
    }

    checkRepetition(data) {
        const set = new Set();
        data.forEach(item => {
            if (set.has(item.student_id))
                item.repeated = true;

            set.add(item.student_id)
        })
        return data;
    }

    render() {
        const bookings = this.checkRepetition(this.props.bookings);
        return (
            <div className="container">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Ammount</th>
                            <th>Ammount with fees</th>
                            <th>Ammount received</th>
                            <th>Quality check</th>
                            <th>Over payment</th>
                            <th>Under payment</th>
                        </tr>
                    </thead>

                    <tbody>
                        {bookings.map(
                            (item) => (
                                <RowComponent book={item} key={item.reference} />
                            )
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}


export default TableComponent