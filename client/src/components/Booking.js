export default class Booking {
   
    ammountTresh = 1000000;
    constructor(data) {
        for (var item in data) {
            this[item] = data[item];
        }

        //No specify how to round numbers, so i round only the ammount with fees
        this.ammount_feeds = this.round(this.ammountWithFees());
        this.quality_errors = this.checkQuality();
    }


    round(num){
        return Math.round(num * 100) / 100
    }

    ammountWithFees() {
        //equal asigned, not especified what happens if ammount equals 1000 or 10000
        if (this.amount < 1000)
            return this.amount * 1.05;
        else if (this.amount >=1000 && this.amount <= 10000)
            return this.amount * 1.03;
        else
            return this.amount * 1.02;
    }

    //Not sure what happens if has two checks 
    checkQuality() {
        const checks = [];

        if (this.amount > this.ammountTresh)
            checks.push("AmountThreshold");

        if (!this.validateEmail())
            checks.push("InvalidEmail")

        if (this.repeated)
            checks.push("DuplicatedPayment")

        return checks;

    }

    validateEmail() {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.email);
    }

}
