import React from 'react';

function LoaderSpiiner() {
    return (
        <div className="container">
            <div className="spinner-border" style={{ marginTop: 30 }} role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <p>Loading</p>
        </div>
    )
}


export default LoaderSpiiner;