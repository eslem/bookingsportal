import React from 'react';
import './App.css';
import TableComponent from './components/TableComponent';
import LoaderSpiner from './components/LoaderSpiner';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { bookings: [], loading: true, error: null }
  }

  componentWillMount() {
    this.loadDocuments();
  }

  getBookings() {
    const url = '/api/bookings';
    return fetch(url).then(response =>
      response.json()
    )
  }

  restart() {
    this.setState({ "loading": true });
    this.loadDocuments();
  }

  async loadDocuments() {
    try {
      const data = await this.getBookings();
      this.setState({ "bookings": data.bookings, "loading": false })
      return data.bookings;
    } catch (e) {
      this.setState({ "error": e, "loading": false })
    }
  }


  render() {
    const { bookings, loading, error } = this.state;
    return (
      <div className="App">
        {loading && <LoaderSpiner ></LoaderSpiner>}
        {!loading && error && <p>Error loading data</p>}
        {
          !loading && !error &&
            <TableComponent bookings={bookings} />
        }
        <p>
          <button className="btn btn-secondary" onClick={() => this.restart()}>Refresh</button>
        </p>
      </div>
    );
  }
}

export default App;
