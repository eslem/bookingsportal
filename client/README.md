# Client Solution

![screenshot](./screenshot.png "Screenshot")


## Stack
- React
- Bootstrap
- Docker


## Demo
To boot up the application for development run (server needs to be running):

```
$ docker-compose build
$ docker-compose up
```

Open on your browser

```
http://localhost:3000
```

## Tests
```
$ yarn test
```
